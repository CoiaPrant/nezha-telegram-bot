package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/CoiaPrant/zlog"
	"github.com/dustin/go-humanize"
	tg "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/gorilla/websocket"
	emoji "github.com/jayco/go-emoji-flag"
)

var bot *tg.BotAPI
var update chan *ServerStatus

func init() {
	update = make(chan *ServerStatus)
}

func main() {
	var token, url, tag string
	var chatid int64
	var messageid int
	var show_origin bool
	{
		zlog.Print("作者 CoiaPrant 联系方式 https://t.me/CoiaPrant")
		zlog.Print("频道 https://t.me/zerocloud")
		zlog.Print("项目地址 https://gitlab.com/CoiaPrant/nezha-telegram-bot")

		var install, h bool
		flag.StringVar(&token, "token", "", "The telegram bot token")
		flag.StringVar(&url, "url", "", "The nezha dashboard url")
		flag.StringVar(&tag, "tag", "", "The nezha servers tag")
		flag.Int64Var(&chatid, "chatid", 0, "The telegram chat-id")
		flag.IntVar(&messageid, "messageid", 0, "The telegram message-id")
		flag.BoolVar(&show_origin, "show-origin", false, "Show dashboard link")
		flag.BoolVar(&install, "install", false, "install as system service")
		flag.BoolVar(&h, "h", false, "Show help")
		flag.Parse()

		if h {
			flag.PrintDefaults()
			return
		}

		if token == "" || chatid == 0 || (messageid == 0 && !install) {
			zlog.Fatal("[Initial] Missing param")
			return
		}

		var err error
		bot, err = tg.NewBotAPI(token)
		if err != nil {
			zlog.Fatal("[Framework] ", err)
			return
		}

		if install {
			msg, err := bot.Send(&tg.MessageConfig{
				BaseChat: tg.BaseChat{
					ChatID:           chatid,
					ReplyToMessageID: 0,
				},
				ParseMode:             "html",
				Text:                  "<strong>服务器状态</strong>\n\n初始化成功",
				DisableWebPagePreview: true,
			})

			if err != nil {
				zlog.Fatal("[Install] ", err)
				return
			}
			RegisterService(token, url, tag, chatid, msg.MessageID, show_origin)
			zlog.Success("安装成功!")
			return
		}
	}

	header := http.Header{}
	header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edge/101.0.1210.53")

	go updateMessage(chatid, messageid, tag, url, show_origin)
	for {
		time.Sleep(time.Second)

		ws, resp, err := websocket.DefaultDialer.Dial(strings.ReplaceAll(url, "http", "ws")+"/ws", header)
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}

		if err != nil {
			zlog.Error(err)
			continue
		}

		handleWebsocket(ws)
	}
}

type ServerStatus struct {
	Now     int64 `json:"now"`
	Servers []struct {
		ID           int         `json:"ID"`
		CreatedAt    time.Time   `json:"CreatedAt"`
		UpdatedAt    time.Time   `json:"UpdatedAt"`
		DeletedAt    interface{} `json:"DeletedAt"`
		Name         string      `json:"Name"`
		Tag          string      `json:"Tag"`
		DisplayIndex int64       `json:"DisplayIndex"`
		Host         struct {
			Platform        string   `json:"Platform"`
			PlatformVersion string   `json:"PlatformVersion"`
			CPU             []string `json:"CPU"`
			MemTotal        uint64   `json:"MemTotal"`
			DiskTotal       uint64   `json:"DiskTotal"`
			SwapTotal       uint64   `json:"SwapTotal"`
			Arch            string   `json:"Arch"`
			Virtualization  string   `json:"Virtualization"`
			BootTime        uint64   `json:"BootTime"`
			CountryCode     string   `json:"CountryCode"`
			Version         string   `json:"Version"`
		} `json:"Host"`
		State struct {
			CPU            float64 `json:"CPU"`
			MemUsed        uint64  `json:"MemUsed"`
			SwapUsed       uint64  `json:"SwapUsed"`
			DiskUsed       uint64  `json:"DiskUsed"`
			NetInTransfer  uint64  `json:"NetInTransfer"`
			NetOutTransfer uint64  `json:"NetOutTransfer"`
			NetInSpeed     uint64  `json:"NetInSpeed"`
			NetOutSpeed    uint64  `json:"NetOutSpeed"`
			Uptime         uint64  `json:"Uptime"`
			Load1          float64 `json:"Load1"`
			Load5          float64 `json:"Load5"`
			Load15         float64 `json:"Load15"`
			TCPConnCount   uint64  `json:"TcpConnCount"`
			UDPConnCount   uint64  `json:"UdpConnCount"`
			ProcessCount   uint64  `json:"ProcessCount"`
		} `json:"State"`
		LastActive time.Time `json:"LastActive"`
	} `json:"servers"`
}

func handleWebsocket(ws *websocket.Conn) {
	if ws == nil {
		return
	}

	for {
		messageType, data, err := ws.ReadMessage()
		if err != nil {
			return
		}

		if messageType != websocket.TextMessage {
			continue
		}

		var stat ServerStatus
		if err = json.Unmarshal(data, &stat); err != nil {
			return
		}

		select {
		case update <- &stat:
		default:
		}
	}
}

func updateMessage(chatid int64, messageid int, tag, origin string, show_origin bool) {
	for range time.Tick(5 * time.Second) {
		stat := <-update
		msg := "<strong>服务器状态</strong> #" + tag
		if tag == "" {
			msg = "<strong>服务器状态</strong> #默认"
		}

		for _, server := range stat.Servers {
			if server.Tag != tag {
				continue
			}

			if server.LastActive.IsZero() {
				continue
			}

			msg += "\n\n"

			if server.LastActive.Add(10 * time.Second).Before(time.Now()) {
				msg += "[离线] " + emoji.GetFlag(server.Host.CountryCode) + server.Name + "\n"
			} else {
				msg += emoji.GetFlag(server.Host.CountryCode) + server.Name + "\n"
			}

			msg += "<pre>"
			msg += fmt.Sprintf("CPU %.2f%% [%v]\n", server.State.CPU, server.Host.Arch)
			msg += fmt.Sprintf("负载 %.2f %.2f %.2f \n", server.State.Load1, server.State.Load5, server.State.Load15)
			msg += fmt.Sprintf("内存 %.0f%% [%v/%v] \n", float64(server.State.MemUsed)/float64(server.Host.MemTotal)*100, humanize.Bytes(server.State.MemUsed), humanize.Bytes(server.Host.MemTotal))
			if server.Host.SwapTotal != 0 {
				msg += fmt.Sprintf("交换 %.0f%% [%v/%v] \n", float64(server.State.SwapUsed)/float64(server.Host.SwapTotal)*100, humanize.Bytes(server.State.SwapUsed), humanize.Bytes(server.Host.SwapTotal))
			}
			msg += fmt.Sprintf("网速 ↓%v/s ↑%v/s \n", humanize.Bytes(server.State.NetInSpeed), humanize.Bytes(server.State.NetOutSpeed))
			msg += fmt.Sprintf("流量 ↓%v ↑%v", humanize.Bytes(server.State.NetInTransfer), humanize.Bytes(server.State.NetOutTransfer))
			msg += "</pre>"
		}

		msg += "\n\n"

		msg += fmt.Sprintf("更新时间: %v \n", time.Now().Format("2006-01-02 15:04:05"))
		if show_origin {
			msg += fmt.Sprintf(`数据来源: <a href="%v">Link</a>`, origin)
		}

		bot.Send(&tg.EditMessageTextConfig{
			BaseEdit: tg.BaseEdit{
				ChatID:    chatid,
				MessageID: messageid,
			},
			ParseMode:             "html",
			Text:                  msg,
			DisableWebPagePreview: true,
		})
	}
}
