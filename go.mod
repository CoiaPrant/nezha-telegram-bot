module nezha-telegram-bot

go 1.18

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/gorilla/websocket v1.5.0
)

require (
	github.com/jayco/go-emoji-flag v0.0.0-20190810054606-01604da018da
	gitlab.com/CoiaPrant/zlog v1.0.0
)
