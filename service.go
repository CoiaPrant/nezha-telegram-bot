package main

import (
	"os"
	"os/exec"
	"strconv"
	"strings"
)

const serviceTemplate = `[Unit]
Description=Nezha Telegram Bot
After=network.target

[Service]
Type=simple
ExecStart={binary} --url "{url}" --token "{token}" --chatid {chatid} --messageid {messageid} --tag "{tag}" {show-origin}
User=root
Restart=always
RestartSec=5s
LimitCPU=infinity
LimitFSIZE=infinity
LimitDATA=infinity
LimitSTACK=infinity
LimitCORE=infinity
LimitRSS=infinity
LimitNOFILE=infinity
LimitAS=infinity
LimitNPROC=infinity
LimitMEMLOCKS=infinity
LimitSIGPENDING=infinity
LimitMSGQUEUE=infinity
LimitRPTRIO=infinity
LimitRTTIME=infinity

[Install]
WantedBy=multi-user.target`

func RegisterService(token, url, tag string, chatid int64, messageid int, show_origin bool) {
	file := strings.ReplaceAll(serviceTemplate, "{binary}", getSelfPath())
	file = strings.ReplaceAll(file, "{url}", url)
	file = strings.ReplaceAll(file, "{token}", token)
	file = strings.ReplaceAll(file, "{chatid}", strconv.FormatInt(chatid, 10))
	file = strings.ReplaceAll(file, "{messageid}", strconv.Itoa(messageid))
	file = strings.ReplaceAll(file, "{tag}", tag)
	if show_origin {
		file = strings.ReplaceAll(file, "{show-origin}", "--show-origin")
	} else {
		file = strings.ReplaceAll(file, "{show-origin}", "")
	}

	os.Remove("/etc/systemd/system/nezha-telegram-bot.service")
	os.WriteFile("/etc/systemd/system/nezha-telegram-bot.service", []byte(file), 0644)
	shellExec("systemctl daemon-reload")
	shellExec("systemctl enable nezha-telegram-bot")
	shellExec("systemctl start nezha-telegram-bot")
}

func shellExec(Cmd string) {
	cmd := exec.Command("/bin/sh", "-c", Cmd)
	cmd.Start()
	cmd.Wait()
}

func getSelfPath() string {
	path, err := os.Executable()
	if err != nil {
		return os.Args[0]
	}
	return path
}
